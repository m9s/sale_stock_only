# -*- coding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.model import fields

__metaclass__ = PoolMeta
__all__ = ['SaleChannel']

STATES = {
    'readonly': ~Eval('active', True),
    }
DEPENDS = ['active']


class SaleChannel:
    __name__ = 'sale.channel'

    show_stock_only = fields.Boolean('Show only products with stock',
        help='Default setting for the display on sales: '
        'Show only products that are available in stock.', states=STATES,
        depends=DEPENDS)
